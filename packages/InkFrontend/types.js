// Action types

export const INK_REQUEST = 'INK_REQUEST'
export const INK_SUCCESS = 'INK_SUCCESS'
export const INK_FAILURE = 'INK_FAILURE'
